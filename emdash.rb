require 'yaml'
require 'faye/websocket'

class Emdash
  def initialize
    # config path
    @userlistpath = ENV["EMDASH_USERLIST"] || File.expand_path("../users.yml", __FILE__)

    # user config is a hash in the form of:
    # {user_name => relay_host_as_uri}
    @users = YAML.load(File.read(@userlistpath))

    # storage for open websockets, a hash in the form of:
    # `{client_socket_id => {:server => server_socket, :authed => bool, :user => username}}`
    @sockets = {}
  end

  def log(msg)
    puts "[#{Time.now}] #{msg}"
    STDOUT.flush
  end

  def call(env)
    if !Faye::WebSocket.websocket?(env)
      return [200, {'Content-Type' => 'text/plain'}, ['']]
    else
      ws = Faye::WebSocket.new(env)

      ws.on :open do
        log "Connection #{ws.object_id} opened"
        @sockets[ws.object_id] = {
          :server => nil,
          :server_uri => nil,
          :compression => "off",
          :init_id => nil,
          :authed => false,
          :sent_auth => false,
          :buffer => [],
          :user => nil,
          :password => nil,
        }
      end

      ws.on :close do
        if @sockets[ws.object_id][:server]
          @sockets[ws.object_id][:server].close
        end

        log "Connection #{ws.object_id} closed"
        @sockets.delete(ws.object_id)
        ws = nil
      end

      ws.on :message do |ev|
        raw_msg = msg = ev.data
        raw_msg = msg = ev.data.pack("C*") if ev.data.kind_of?(Array)

        unless @sockets[ws.object_id][:sent_auth]
          if msg.downcase =~ /^(?:\((.+)\)\s+)?init/
            @sockets[ws.object_id].merge!({ :init_id => $1 }) if $1
            msg.gsub! /^\(.+\)\s+/, ""

            msg.split(" ")[-1].split(",").each do |s|
              next unless s.index("=")
              k, v = s.split("=")

              if k.downcase == "password"
                next unless v.index(":")
                user, pass = v.split(":")

                if @users.key? user.downcase
                  @sockets[ws.object_id].merge!({
                    :server_uri => @users[user.downcase],
                    :authed => true,
                    :user => user.downcase,
                    :password => pass,
                  })

                  log "Connection #{ws.object_id} authenticated as #{user.downcase}"
                else
                  log "Connection #{ws.object_id} failed to authenticate"
                  ws.close
                  next
                end
              elsif k.downcase == "compression"
                @sockets[ws.object_id].merge!({
                  :compression => v,
                })
              end
            end

            if @sockets[ws.object_id][:authed]
              data = @sockets[ws.object_id]
              log "Connection #{ws.object_id} connecting to backend at #{data[:server_uri]}"

              socket = Faye::WebSocket::Client.new(data[:server_uri])
              @sockets[ws.object_id].merge!({
                :server => socket,
              })

              socket.on :open do |ev|
                out = "init compression=#{data[:compression]},password=#{data[:password]}\n"
                out = "(#{data[:init_id]}) #{out}" if data[:init_id]

                socket.send out
                @sockets[ws.object_id].merge!({:sent_auth => true})

                while @sockets[ws.object_id][:buffer].length > 0
                  shifted_msg = @sockets[ws.object_id][:buffer].shift
                  socket.send(shifted_msg)
                end
              end

              socket.on :message do |ev|
                ws.send(ev.data)
              end

              socket.on :close do |ev|
                if @sockets.key? ws.object_id
                  log "Connection #{ws.object_id} backend socket closed"
                  ws.close
                end
              end
            end
          else
            @sockets[ws.object_id][:buffer] << raw_msg
          end
        else
          if msg.downcase =~ /^(?:\(.+\)\s+)?quit/
            log "Connection #{ws.object_id} sent quit, closing"
            @sockets[ws.object_id][:server].send("quit\n")
            @sockets[ws.object_id][:server].close
            ws.close
            next
          end

          tosend = "#{raw_msg}#{raw_msg.end_with?("\n") ? "" : "\n"}"
          @sockets[ws.object_id][:server].send(tosend)
        end
      end

      return ws.rack_response
    end
  end
end
