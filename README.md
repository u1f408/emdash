# emdash

Emdash is a [weechat] relay authentication proxy, that allows for running multiple
weechat relays on the same port (or, subdomain, if you're proxying through a web
server).

## Setting up

Clone, run `bundle install`, and then edit the `users.yml` file:

```yaml
username: "ws://localhost:4444/weechat"
```

`username` is the username you wish to connect to the relay client with. The URL
is a websocket URL to the relay server - which must end in `/weechat` for 
weechat's websocket server to work.

## Running

```sh
$ bundle exec puma config.ru -p $PORT
```

You may wish to run this on a port that isn't exposed to the internet, and proxy
through a web server the same way that you'd proxy a standard weechat relay.

## Using

Connect to the port you specified above with any weechat relay client. Your
password will now be in the format `username:password` - where `username` is the
username set in the configuration for the relay you wish to use, and `password`
is the password set in weechat (the `relay.network.password` option).

## License

MIT, see [LICENSE]

[weechat]: http://weechat.org
[LICENSE]: ./LICENSE

