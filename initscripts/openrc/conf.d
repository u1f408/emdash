EMDASH_USER="emdash"
EMDASH_HOME="$(getent passwd $EMDASH_USER | cut -d: -f6)"
EMDASH_DIR="$EMDASH_HOME/emdash/"
EMDASH_PIDFILE="$EMDASH_HOME/emdash.pid"
EMDASH_PUMA_OPTS="--quiet --daemon --pidfile $EMDASH_PIDFILE --environment production --bind unix://$EMDASH_HOME/emdash.sock"

