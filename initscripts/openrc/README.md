# openrc initscripts

Assumes:

* that an `emdash` user exists
* that the repository is cloned into the directory `emdash` in the `emdash` user's home directory

```
cp init.d /etc/init.d/emdash
cp conf.d /etc/conf.d/emdash
$EDITOR /etc/conf.d/emdash
chmod +x /etc/init.d/emdash
rc-service emdash start
```
