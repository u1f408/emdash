#!/sbin/openrc-run

name="emdash"
description="weechat websocket authentication proxy"

depend() {
	need net localmount
	after bootmisc
}

start() {
	ebegin "Starting $name"

	start-stop-daemon \
		--start \
		--chdir "$EMDASH_DIR" \
		--user "$EMDASH_USER" \
		--pidfile "$EMDASH_PIDFILE" \
		--exec /usr/bin/bundle \
		-- exec puma config.ru $EMDASH_PUMA_OPTS

	eend $?
}

stop() {
	ebegin "Stopping $name"

	start-stop-daemon \
		--stop \
		--chdir "$EMDASH_DIR" \
		--user "$EMDASH_USER" \
		--pidfile "$EMDASH_PIDFILE" \
		--exec /usr/bin/bundle 

	eend $?
}
